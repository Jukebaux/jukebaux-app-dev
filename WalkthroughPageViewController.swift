//
//  WalkthroughPageViewController.swift
//  Jukebaux
//
//  Created by Daryll Santos on 10/12/16.
//  Copyright © 2016 Jukebaux, LLC. All rights reserved.
//

import Foundation
import UIKit

class walkthroughPageViewController: UIPageViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
    }
}

// MARK: UIPageViewControllerDataSource

extension walkthroughPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return nil
    }
    
}

private(set) var orderedViewControllers: [UIViewController] = {
    return [walkthroughPageViewController(coder: "1"),
            walkthroughPageViewController(coder: "2"),
            walkthroughPageViewController(coder: "3")]
}()

private func newColoredViewController(color: String) -> UIViewController {
    return UIStoryboard(name: "Main", bundle: nil) .
        instantiateViewController(withIdentifier: "\(color)ViewController")
    
}

func viewDidLoad() {
    super.viewDidLoad()
    
    dataSource = self
    
    if let firstViewController = orderedViewControllers.first {
        firstViewController([firstViewController],
                           direction: .Forward,
                           animated: true,
                           completion: nil)
    }
}

